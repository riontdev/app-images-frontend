import axios from 'axios'
import store from '../store'
import router from '../router'

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

axios.interceptors.request.use(request => {
  if (store.getters.authToken) {
    request.headers.common['Authorization'] = `Bearer ${store.getters.authToken}`
  }
  return request
})

axios.interceptors.response.use(response => response, error => {
  const { status } = error.response

  if (status >= 500) {
    store.dispatch('responseMessage', {
      type: 'error',
      text: error.msg,
      title: error.msg,
      modal: true
    })
  }

  if (status === 401 && store.getters.authCheck) {
    store.dispatch('responseMessage', {
      type: 'warning',
      text: error.msg,
      title: error.msg,
      modal: true
    })
    .then(async () => {
      await store.dispatch('logout')

      router.push({ name: 'login' })
    })
  }

  return Promise.reject(error)
})