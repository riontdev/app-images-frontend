import LoginForm from '../components/LoginForm.vue';
import CallBack from '../components/CallBack.vue';
import AboutView from '../views/About.vue';

export default ({ authGuard, guestGuard }) => [
  
    // Authenticated routes.
    ...authGuard([
      { path: '/home', name: 'home', component: AboutView },
    ]),
  
    // Guest routes.
    ...guestGuard([
    { path: '/login', name: 'login', component: LoginForm  },   
    { path: '/callback', name: 'callback', component: CallBack  },  
      //{ path: '/admin/login', name: 'login', component: require('~/pages/auth/login.vue') },
      //{ path: '/admin//register', name: 'register', component: require('~/pages/auth/register.vue') },
     // { path: '/admin//password/reset', name: 'password.request', component: require('~/pages/auth/password/email.vue') },
      //{ path: '/admin//password/reset/:token', name: 'password.reset', component: require('~/pages/auth/password/reset.vue') }
    ]),
   { path: '*', component: LoginForm }
  ]