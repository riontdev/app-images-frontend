import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

const baseURL = process.env.VUE_APP_API_ENDPOINT;
// state
export const state = {
  user: null,
  token: Cookies.get('token')
}

// mutations
export const mutations = {
  [types.SAVE_TOKEN] (state, { token, remember }) {
    state.token = token
    Cookies.set('token', token, { expires: remember ? 365 : null })
  },

  [types.FETCH_USER_SUCCESS] (state, { user }) {
    state.user = user
  },

  [types.FETCH_USER_FAILURE] (state) {
    state.token = null
    Cookies.remove('token')
  },

  [types.LOGOUT] (state) {
    state.user = null
    state.token = null

    Cookies.remove('token')
  },

  [types.UPDATE_USER] (state, { user }) {
    state.user = user
  }
}

// actions
export const actions = {
  saveToken ({ commit }, payload) {
    commit(types.SAVE_TOKEN, payload)
  },

  async loginGoogle ({commit}) {
    try {
      const data = await axios.get('/api/auth/google/')
      //console.log(e);
    } catch (e) { 
      //
    }

    commit(types.LOGOUT)
  },

  async fetchUser ({ commit }) {
    try {
      const { data } = await axios.get(baseURL + 'api/auth/user')

      commit(types.FETCH_USER_SUCCESS, { user: data.data })
    } catch (e) {
      commit(types.FETCH_USER_FAILURE)
    }
  },

  async updateUser ({ commit }, payload) {
    commit(types.UPDATE_USER, payload)
  },

  async logout ({ commit }) {
    try {
      await axios.get('/api-admin/logout')
    } catch (e) { 
      //console.log(e);
    }

    commit(types.LOGOUT)
  }
}

// getters
export const getters = {
  authUser: state => state.user,
  authToken: state => state.token,
  authCheck: state => state.user !== null
}